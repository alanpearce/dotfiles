#!/bin/sh

set -eu

if [ -z "$1" ]
then
  echo "Usage: $0 <machine-file.nix>"
  exit 1
fi

NIXDIR="$HOME/.config/nixpkgs"

if [ ! -f "$1" ]
then
  echo "$1 does not exist"
  exit 1
fi

ln -s $PWD $NIXDIR
ln -s $1 home.nix
