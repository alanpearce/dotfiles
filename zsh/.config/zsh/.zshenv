if [[ $SHLVL -eq 1 || -n $DISPLAY ]]
then
  if [[ -f $ZDOTDIR/zshenv.local ]]
  then
    . $ZDOTDIR/zshenv.local
  fi

  if [[ -f $ZDOTDIR/zshenv.private ]]
  then
    . $ZDOTDIR/zshenv.private
  fi

  if [[ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]]
  then
    . $HOME/.nix-profile/etc/profile.d/nix.sh;
    export NIX_LINK
  fi

  case $OSTYPE in
    darwin*)
      os=darwin
      ;;
    linux-gnu)
      os=linux
      ;;
    freebsd*)
      os=freebsd
      ;;
    *)
      os=unknown
      ;;
  esac

  case $MACHTYPE in
    *64)
      arch=amd64
      ;;
    *)
      arch=386
      ;;
  esac

  if [[ ${path[(I)$HOME/.local/bin ]} ]]
  then
    path+=($HOME/.local/bin)
  fi

  if [[ ${path[(I)$HOME/go/bin ]} ]]
  then
    path+=($HOME/go/bin)
  fi

  if [[ $HOST =~ satoshi ]]
  then
    EMAIL=alan@satoshipay.io
  else
    EMAIL=alan@alanpearce.eu
  fi

  export GTAGSCONF=~/.globalrc
  export GTAGSLABEL=ctags

  export GHQ_ROOT="$HOME/projects:$HOME/go/src:$HOME/quicklisp/local-projects"
fi
