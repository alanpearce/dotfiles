#!/usr/bin/env zsh

mkdir $HOME/.zplugin
git clone https://github.com/psprint/zplugin.git $HOME/.zplugin/bin

zcompile $HOME/.zplugin/bin/zplugin.zsh

mkdir -p $HOME/.cache/zsh/

