# -*- mode: sh; -*-
if [[ -f "$HOME/.zplugin/bin/zmodules/Src/zdharma/zplugin.so" ]]
then
  module_path+=( "/Users/alan/.zplugin/bin/zmodules/Src" )
  zmodload zdharma/zplugin
else
  echo "Zplugin module is not installed" >&2
fi

source $HOME/.zplugin/bin/zplugin.zsh
autoload -Uz _zplugin
(( ${+_comps} )) && _comps[zplugin]=_zplugin

HISTSIZE=10000
SAVEHIST=20000
HISTFILE=${XDG_CACHE_HOME:=$HOME/.cache}/zsh/history
[[ -d ${HISTFILE:h} ]] || mkdir -p ${HISTFILE:h}
setopt extended_history
setopt hist_save_no_dups
setopt hist_ignore_space
unsetopt share_history
setopt inc_append_history_time
setopt transient_rprompt

WORDCHARS=${${WORDCHARS//[-.=]}//[\/]}

alias ec=emacsclient

ls='\ls'
gnu_ls_options="-v --group-directories-first --color=auto"
gnu_ls_isodate="--time-style=long-iso"
bsd_ls_options="-p"
bsd_ls_isodate="-D '%F %k:%M'"

case $os in
  darwin)
    if [[ -n $commands[gls] ]]
    then
      ls='\gls'
      ls_options=$gnu_ls_options
      ls_isodate=$gnu_ls_isodate
    else
      export CLICOLOR=1
      ls_options=$bsd_ls_options
    fi
    ;;
  freebsd)
    ls_options=$bsd_ls_options
    ls_isodate=$bsd_ls_isodate
    ;;
  linux)
    ls_options=$gnu_ls_options
    ls_isodate=$gnu_ls_isodate
    ;;
esac
alias watch="watch " # enable watch with aliases
alias l="${ls} ${ls_options} -Bp"
alias l1="${ls} ${ls_options} -1"
alias ls="${ls} ${ls_options} -hF"
alias la="${ls} ${ls_options} -hA"
alias ll="${ls} ${ls_options} ${ls_isodate} -hl"
alias lal="ll -A"
alias lla="lal"
alias llr="ll -t"

alias https="http --default-scheme https"
alias kns="kubens"
alias kx="kubectx"

alias ava="pnpx ava"
alias avt="pnpx ava --tap"
alias avat="pnpx ava --tap"
alias pino="pino-pretty"
alias mocha="pnpx mocha"
alias prettier="pnpx prettier"
alias standard="pnpx standard"
alias tsc="pnpx tsc"
alias tslint="pnpx tslint"
alias tsnode="pnpx ts-node"
alias wprop="xprop | egrep '^WM_(CLASS|NAME|WINDOW_ROLE|TYPE)'"
zmodload zsh/terminfo

bindkey -e
bindkey '\e[3~' delete-char

bindkey '\C-hd' describe-key-briefly

dc () {
  if [[ -x ./docker-compose ]]
  then
    ./docker-compose "$@"
  else
    docker-compose "$@"
  fi
}
compdef '_dispatch docker-compose docker-compose' dc

backward-argument () {
  local WORDCHARS="\!\`~#@$%^&*()-_=+[{]}\|;:,<.>/?\'\""
  zle backward-word
}

forward-argument () {
  local WORDCHARS="\!\`~#@$%^&*()-_=+[{]}\|;:,<.>/?\'\""
  zle forward-word
}

backward-kill-argument () {
  local WORDCHARS="\!\`~#@$%^&*()-_=+[{]}\|;:,<.>/?\'\""
  zle backward-kill-word
}

kill-argument () {
  local WORDCHARS="\!\`~#@$%^&*()-_=+[{]}\|;:,<.>/?\'\""
  zle kill-word
}

zle -N backward-argument
zle -N forward-argument
zle -N kill-argument
zle -N backward-kill-argument
bindkey '\e^b' backward-argument
bindkey '\e^f' forward-argument
bindkey '\e^d' backward-kill-argument
bindkey '\e^k' kill-argument

sort=${commands[gsort]:-$commands[sort]}

ds () {
  du -hd1 $1 | $sort -h
}

# returns the first ghq root, whereas $GHQ_ROOT returns all
hash -d p=$(ghq root)
hash -d go=${GOPATH:-$HOME/go}

zle -C hist-complete complete-word _generic
zstyle ':completion:hist-complete:*' completer _history
bindkey '\e ' hist-complete

zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' completer _expand _complete _match

# Plugins

zplugin ice blockf wait'!' lucid
zplugin load "zsh-users/zsh-completions"
zplugin ice wait'[[ -n ${ZLAST_COMMANDS[(r)k*]} || "$PWD" =~ "kubernetes" ]]' lucid
zplugin load alanpearce/kubectl-aliases
zplugin ice wait'1' lucid atinit'alias cdg=cd-gitroot'
zplugin load "mollifier/cd-gitroot"
zplugin ice wait'[[ -n ${ZLAST_COMMANDS[(r)rm*]} ]]' lucid
zplugin load "MikeDacre/careful_rm"

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=7"
zplugin ice wait'1' lucid if'[[ -z $SSH_CLIENT ]]'
zplugin load "zsh-users/zsh-autosuggestions"

zplugin ice wait'1' lucid
zplugin load "arzzen/calc.plugin.zsh"

# provides the title function and macOS title-folder sync
zplugin ice if'[[ $os -eq darwin ]]'
zplugin snippet "OMZ::lib/functions.zsh"
zplugin ice if'[[ $os -eq darwin ]]'
zplugin snippet "OMZ::lib/termsupport.zsh"

zplugin ice if'[[ $os -eq darwin ]]' wait'5' lucid
zplugin load "unixorn/tumult.plugin.zsh"

zplugin ice wait'[[ -n ${ZLAST_COMMANDS[(r)npm*]} ]]' lucid
zplugin load "lukechilds/zsh-better-npm-completion"

zplugin ice if'[[ (-n $commands[nix-env] && -z $commands[nixos-rebuild]) ]]' wait'[[ -n ${ZLAST_COMMANDS[(r)nix-shell*]} ]]' lucid
zplugin load "chisui/zsh-nix-shell"

zplugin ice if'[[ -n $commands[ghq] ]]'
zplugin load "${GOPATH:=$HOME/go}/src/github.com/motemen/ghq/zsh"

zplugin ice wait'!2' lucid
zplugin load "MichaelAquilina/zsh-you-should-use"

if [[ -n $commands[gcloud] ]]
then
   autoload bashcompinit
   bashcompinit
   source ${$(readlink =gcloud):h}/../etc/bash_completion.d/gcloud.inc
fi

if [[ -n $commands[helm] ]]
then
  autoload _helm
  compdef _helm helm
fi


# General configuration

if [[ -n $commands[gpg2] && -z $commands[gpg] ]]
then
  alias gpg=gpg2
fi

if [[ -n $commands[lunchy] ]]
then
  LUNCHY_DIR=$(dirname $(gem which lunchy))/../extras
  if [ -f $LUNCHY_DIR/lunchy-completion.zsh ]; then
    . $LUNCHY_DIR/lunchy-completion.zsh
  fi
fi

PROMPT='%B%F{green}%n %F{blue}%~%b
%# '
if [[ $TERM == "dumb" ]]
then
  unsetopt zle
else
  AGKOZAK_PROMPT_DIRTRIM=0
  AGKOZAK_LEFT_PROMPT_ONLY=${+SSH_CLIENT}

  check_kubectl_context () {
    if [[ "$PWD" =~ /kubernetes ]]
    then
      git_branch=$(git rev-parse --abbrev-ref HEAD | tr '[:upper:]' '[:lower:]')
      kubectl_context=$(kubectl config current-context)
      kubectl_namespace=$(kubectl config view -o jsonpath="{.contexts[?(@.name==\"${kubectl_context}\")].context.namespace}")
      case $git_branch in
        staging)
          kubectl_release=satoshipay-${kubectl_namespace}-staging
          ;;
        production)
          kubectl_release=satoshipay-${kubectl_namespace}
          ;;
        master)
          kubectl_release=
          ;;
        *)
          kubectl_release=story-${git_branch}-${kubectl_namespace}
      esac
      if [[ $kubectl_context =~ production ]]
      then
        kubectl_colour=red
      else
        kubectl_colour=yellow
      fi
        prompt_context="%F{$kubectl_colour}[${kubectl_context}:${kubectl_namespace}]%f"
    else
      prompt_context=
    fi
  }
  precmd_functions+=(check_kubectl_context)
  AGKOZAK_CUSTOM_RPROMPT='${prompt_context}%(3V.%F{${AGKOZAK_COLORS_BRANCH_STATUS}%3v%f.)'
fi
zplugin load agkozak/agkozak-zsh-prompt

zplugin ice wait'!' lucid
zplugin light "mollifier/anyframe"
if [[ -n $commands[fzf] ]]
then
  zplugin snippet "https://github.com/junegunn/fzf/raw/master/shell/key-bindings.zsh"
  export FZF_CTRL_T_COMMAND='
    (git ls-tree -r --name-only HEAD ||
	       fd --hidden --follow --exclude ".git" . |
	             sed s/^..//) 2> /dev/null'
  export FZF_ALT_C_COMMAND='fd --type d --hidden --follow --exclude ".git" .'
  export FZF_DEFAULT_COMMAND=$FZF_CTRL_T_COMMAND

  bindkey '^t' transpose-chars
  bindkey '^x^f' fzf-file-widget

  zstyle ":anyframe:selector:fzf:" command "fzf --height 40%"

  bindkey '\es' anyframe-widget-cd-ghq-repository
  bindkey '^x^k' anyframe-widget-kill
fi

unsetopt flow_control       # Let me use ^S and ^Q

zplugin ice wait'!' if'[[ -z $SSH_CLIENT ]]' lucid atinit'zpcompinit; zpcdreplay -q' lucid
zplugin load "zdharma/fast-syntax-highlighting"

