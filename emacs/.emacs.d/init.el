;;; init --- user init file -*- no-byte-compile: t -*-
;;; Commentary:
;; Entry point for Emacs init.
;; Ensures that main init code is up-to-date by loading the newest version.
;;
;;; Code:
(setq inhibit-startup-echo-area-message "alan")
(setq load-prefer-newer t
      package-user-dir (concat "~/.emacs.d/packages/" emacs-version "/elpa")
      package-enable-at-startup nil
      package-archives nil)

(package-initialize)
(require 'auto-compile nil :noerror)

(defun reload-user-init-file ()
  "Reload init file."
  (interactive)
  (load-file user-init-file))

(when (featurep 'auto-compile)
  (auto-compile-on-load-mode))

(let ((gc-cons-threshold most-positive-fixnum))
  (load (expand-file-name "main.el" user-emacs-directory) :nomessage t))
;;; init ends here
