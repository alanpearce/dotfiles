{ config, pkgs, ... }:

{
  imports = [
    ./modules/base.nix
    ./modules/dunst.nix
    ./modules/emacs.nix
    ./modules/git.nix
    ./modules/gnupg.nix
    ./modules/i3.nix
    ./modules/nix.nix
    ./modules/rofi.nix
    ./modules/sxhkd.nix
    ./modules/tabnine.nix
    ./modules/xresources.nix
    ./modules/zsh.nix
  ];
}
