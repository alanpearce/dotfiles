{ config, pkgs, ... }:

{
  imports = [
    ./modules/autorandr.nix
    ./modules/base.nix
    ./modules/dunst.nix
    ./modules/emacs.nix
    ./modules/git.nix
    ./modules/gnupg.nix
    ./modules/i3.nix
    ./modules/isync.nix
    ./modules/msmtp.nix
    ./modules/nix.nix
    ./modules/npm.nix
    ./modules/rofi.nix
    ./modules/sxhkd.nix
    ./modules/tabnine.nix
    ./modules/trezor.nix
    ./modules/xresources.nix
    ./modules/zsh.nix
  ];
}
