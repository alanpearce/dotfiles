{ config, pkgs, ... }:

{
  xdg.configFile.TabNine = {
    recursive = true;
    source = ../tabnine/.config/TabNine;
  };
}
