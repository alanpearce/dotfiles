{ config, pkgs, ... }:

{
  home.file.".ledgerrc".source = ../ledger/.ledgerrc;
}
