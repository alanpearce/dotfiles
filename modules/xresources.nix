{ config, pkgs, ... }:

{
  home.file.".xresources" = {
    recursive = true;
    source = ../xresources/.xresources;
  };
}
