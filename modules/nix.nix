{ config, pkgs, ... }:

{
  nixpkgs.config = import ../nix/.config/nixpkgs/config.nix;
  xdg.configFile."nixpkgs.config.nix".source = ../nix/.config/nixpkgs/config.nix;
}
