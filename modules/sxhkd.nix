{ config, pkgs, ... }:

{
  xdg.configFile.sxhkd = {
    recursive = true;
    source = ../sxhkd/.config/sxhkd;
  };
}
