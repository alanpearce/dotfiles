{ config, pkgs, ... }:

{
  xdg.configFile.git = {
    recursive = true;
    source = ../git/.config/git;
  };
}
