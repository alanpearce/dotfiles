{ config, pkgs, ... }:

{
  home.file.".gnupg" = {
    recursive = true;
    source = ../gnupg/.gnupg;
  };
}
