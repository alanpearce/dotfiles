{ config, pkgs, ... }:

{
  home.file.".emacs.d" = {
    recursive = true;
    source = ../emacs/.emacs.d;
  };
  home.file.".local/share/applications/emacsclient.desktop".source = ../emacs/.local/share/applications/emacsclient.desktop;
}
