{ config, pkgs, ... }:

{
  xdg.configFile.dunst = {
    recursive = true;
    source = ../dunst/.config/dunst;
  };
}
