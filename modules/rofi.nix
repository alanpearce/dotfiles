{ config, pkgs, ... }:

{
  xdg.configFile.rofi = {
    recursive = true;
    source = ../rofi/.config/rofi;
  };
  xdg.configFile.rofi-pass = {
    recursive = true;
    source = ../rofi/.config/rofi-pass;
  };
}
