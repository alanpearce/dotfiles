{ config, pkgs, ... }:

{
  home.file.".msmtprc".source = ../msmtp/.msmtprc;
}
