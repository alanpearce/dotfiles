{ config, pkgs, ... }:

{
  xdg.configFile.i3 = {
    recursive = true;
    source = ../i3/.config/i3;
  };
  xdg.configFile.i3status = {
    recursive = true;
    source = ../i3/.config/i3status;
  };
}
