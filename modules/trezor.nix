{ config, pkgs, ... }:

{
  home.file.".ssh/agent.config" = {
    text = ''
      ecdsa-curve-name = ed25519
    '';
  };
}
