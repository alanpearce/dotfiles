{ config, pkgs, ... }:

{
  home.file = {
    ".rm_recycle_home".text = ""; # use trash automatically in home directory
    ".zshenv".text = builtins.readFile ../zsh/.zshenv;
  };
  xdg.configFile.zsh = {
    recursive = true;
    source = ../zsh/.config/zsh;
  };
}
