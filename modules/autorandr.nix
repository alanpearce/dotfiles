{ config, pkgs, ... }:

{
  xdg.configFile.autorandr = {
    recursive = true;
    source = ../autorandr/.config/autorandr;
  };
}
