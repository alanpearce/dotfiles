{ config, pkgs, ... }:

{
  home.file.".mbsyncrc".source = ../isync/.mbsyncrc;
}
