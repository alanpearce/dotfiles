{ config, pkgs, ... }:

{
  home.file.".npmrc".source = ../javascript/.npmrc;
}
